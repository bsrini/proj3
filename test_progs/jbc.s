/*
	My test, want to test basics of forwarding
*/
	data = 0x1000
	lda $r4, data
	lda $r1, 5
	lda $r2, 6
	addq $r1,$r2,$r5 /*r5 = 11 -> Standard  test*/
	lda $r1, 0
	lda $r2, 4
	lda $r2, 3
	addq $r2,$r1,$r6 /*r6 = 3-> Test gets correct value w/ mult values in flight */
	addq $r6,$r2,$r5 /*r5 = 6 -> test depends on prev operation result*/
	addq $r5,$r6,$r7 /* r7 = 9 -> 3 in a row dependencies */
	addq $r1,$r2,$r8 /* r8 = 3 -> non dependenent after */
	addq $r4, $r4, $r4
	stq $r8, 0($r4)  /*mem[4096] = 3 -> st dep on prev */
	nop
	nop
	nop
	nop
	lda $r1, 1
	ldq $r2, 0($r4)
	addq $r1,$r2,$r3
	addq $r4,$r4,$r4
	stq $r3, 0($r4)
	nop
	call_pal        0x555
