/*
	See if the id_hazards_out goes high for the right data dependnecies. 
*/
	data = 0x1000
	lda     $r1,1
	lda     $r2,2
	lda		$r4, data
	addq	$r1, $r2, $r3
	addq $r3, $r3, $r6
	addq $r1, $r6, $r7
	stq $r7, 0($r4)
	ldq $r5, 0($r4)
	addq $r5, $r6
	call_pal        0x555
/*
  HAZARD DETECTION
  bit 0: reg B is a hazard, forward from ex_mem pipeline register
  bit 1: reg B is a hazard, forward from mem_wb pipeline register
  bit 2: reg A is a hazard, forward from ex_mem pipeline register
  bit 3: reg A is a hazard, forward from mem_wb pipeline register
  */
