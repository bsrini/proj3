/*
	My test, want to test basics of data hazards
*/
	data = 0x1000
	lda $r3, 3
	lda $r1, data
	stq $r3, 0($r1)	
	ldq $r2, 0($r1)
	addq $r2,$r2,$r2
	addq $r1,$r1,$r1
	stq $r2, 0($r1)
	call_pal        0x555
